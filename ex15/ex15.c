#include <stdio.h>

int main(int argc, char *argv[]) {
  // create two arrays we care about
  int ages[] = { 23, 43, 23, 89 };
  char *names[] = {
    "Pyc", "Jef",
    "Ciro", "Lea",
  };

  // safely get the size of ages
  int count = sizeof(ages) / sizeof(int);
  int i = 0;

  // first way using indexing
  for (i = 0; i < count; i++) {
    printf("%s has %d years alive. \n", names[i], ages[i]);
  }

  printf("----\n");

  // setup the pointers to the start of the arrays
  int *p_age = ages;
  char **p_name = names;

  // second way using pointers
  for (i = 0; i < count; i++) {
    printf("%s is %d years old. \n", *(p_name + i), *(p_age + i));
  }

  printf("----\n");

  // thid way using pointers
  for (i = 0; i < count; i++) {
    printf("%s is %d years old.\n", p_name[i], p_age[i]);
  }

  printf("----\n");

  // fourh way with pointers in a stupid complex way
  for (p_name = names, p_age = ages; (p_age - ages) < count; p_name++, p_age++) {
    printf("%s is %d years old.\n", *p_name, *p_age);
  }

  return 0;
}
