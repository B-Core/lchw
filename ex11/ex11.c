#include <stdio.h>

int main(int argc, char *argv[]) {
  int numbers[4] = { 0 };
  char name[5] = { 'a' };

  // first, print them out raw
  printf("numbers each : %d %d %d %d\n", numbers[0],numbers[1],numbers[2],numbers[3]);

  printf("name each : %c %c %c %c\n", name[0],name[1],name[2],name[3]);

  printf("name : %s\n", name);

  // setup the numbers
  numbers[0] = 0;
  numbers[1] = 1;
  numbers[2] = 2;
  numbers[3] = 3;

  // setup the name
  name[0] = 'C';
  name[1] = 'i';
  name[2] = 'r';
  name[3] = 'o';
  name[4] = '\0';

  // then print them out initialized
  printf("numbers each : %d %d %d %d\n", numbers[0],numbers[1],numbers[2],numbers[3]);

  printf("name each : %c %c %c %c\n", name[0],name[1],name[2],name[3]);

  printf("name : %s\n", name);

  // another way to use name, which is the good way
  char *another = "Ciro";

  printf("another each : %c %c %c %c\n", another[0],another[1],another[2],another[3]);

  printf("another : %s\n", another);

  return 0;
}
