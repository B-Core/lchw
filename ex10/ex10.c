#include <stdio.h>

void isLetterVowel(char letter) {
  switch(letter) {
    case 'a' :
    case 'A' :
    case 'e' :
    case 'E' :
    case 'i' :
    case 'I' :
    case 'o' :
    case 'O' :
    case 'u' :
    case 'U' :
      printf("%c is a vowel.\n", letter);
      break;
    default :
      printf("%c is not a vowel.\n", letter);
  }
}

int main(int argc, char *argv[]) {
  if(argc < 2) {
    printf("ERROR : You need 1 or more arguments.\n");
    // this how you abort a program
    return 1;
  }

  int args = 1;
  int letter_index = 0;

  for(args = 1; args < argc; args++) {
    printf("%d : %s\n", args, argv[args]);
    for(letter_index = 0; argv[args][letter_index] != '\0';letter_index++) {
      isLetterVowel(argv[args][letter_index]);
    }
  }

  return 0;
}
