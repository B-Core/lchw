#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

struct Person {
  char *name;
  int age;
  double height;
  int weight;
};

struct Person *create_Person(char *name, int age, double height, int weight) {
  struct Person *who = malloc(sizeof(struct Person));
  assert(who != NULL);

  who->name = strdup(name);
  who->age = age;
  who->height = height;
  who->weight = weight;

  return who;
}

void destroy_Person(struct Person *who) {
  assert(who != NULL);
  free(who->name);
  free(who);
}

void print_Person(struct Person *who) {
  printf("\n");
  printf("Name %s\n", who->name);
  printf("Age %d\n", who->age);
  printf("Height %.02f\n", who->height);
  printf("Weight %d\n", who->weight);
}

int main(int argc, char **argv) {
  struct Person *ciro = create_Person("Ciro DE CARO", 25, 1.86, 63);

  struct Person *julie = create_Person("Julie Battaglia", 22, 1.62, 66);

  printf("Ciro is at memory location %p\n", ciro);
  printf("Julie is at memory location %p\n", julie);
  
  print_Person(ciro);
  print_Person(julie);

  // make everyone age 20 years and print them again
  ciro->age += 20;
  ciro->height -= 0.02;
  ciro->weight += 10;
  print_Person(ciro);

  julie->age += 20;
  julie->weight += 10;
  print_Person(julie);

  // destroy them so we clean up
  destroy_Person(ciro);
  destroy_Person(julie);

  return 0;
}