CFLAGS=-Wall -g
objects=$(patsubst %.c,%,$(wildcard ex*/*))

all: $(objects)

clean:
	rm -rf ex*/ex*.exe ex*/ex*.o
