# include <stdio.h>

 int main(int argc, char *argv[]) {
 int i = 0;

 // go through each string in argv
 for (i = 1; i < argc; i++) {
   printf("arg %d: %s\n", i, argv[i]);
 }

 // let's make our own array of strings
 /*
 In C you make an array of strings by combining the char *str = "blah" syntax with the
char str[] = {'b','l','a','h'} syntax to construct a two-dimensional array. The syntax
char *states[] = {...} on line 14 is this two-dimensional combination, each string being
one element, and each character in the string being another.
 */
 char *states[] = {
   "California", "Oregon",
   "Washington", "Texas"
 };

 int num_states = 4;

 for (i = 0; i < num_states; i++) {
   printf("state %d: %s\n", i, states[i]);
 }

 return 0;
}
