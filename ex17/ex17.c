#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <stdarg.h> // for optionnal arguments in functions
#include <string.h>

#define MAX_DATA 512
#define MAX_ROWS 100

struct Address {
  int id;
  int set;
  char name[MAX_DATA];
  char email[MAX_DATA];
};

struct Database {
  struct Address rows[MAX_ROWS];
};

struct Connection {
  FILE *file;
  struct Database *db;
};

void close_Database(struct Connection *conn);

void die(const char *message, ...) {
  va_list args; // initiate arguments list with ... as args
  va_start(args, 0); // start arguments consumption at index 0
  struct Connection *conn = va_arg(args, struct Connection *); // extract variable of specified type
  if (errno) {
    perror(message);
  } else {
    printf("ERROR: %s\n", message);
  }
  close_Database(conn);
  exit(1);
}

void print_Addresse(struct Address *addr) {
  printf("%d %s %s\n", addr->id, addr->name, addr->email);
}

void load_Database(struct Connection *conn) {
  int rc = fread(conn->db, sizeof(struct Database), 1, conn->file);
  if (rc != 1) {
    die("Failed to load database", conn);
  }
}

struct Connection *open_Database(const char *filename, char mode) {
  struct Connection *conn = malloc(sizeof(struct Connection));

  if(!conn) {
    die("Memory error !");
  }

  conn->db = malloc(sizeof(struct Database));
  if (!conn->db) {
    die("Memory error !", conn);
  }

  if(mode == 'c') {
    conn->file = fopen(filename, "w");
  } else {
    conn->file = fopen(filename, "r+");

    if (conn->file) {
      load_Database(conn);
    }
  }

  if (!conn->file) {
    die("Failed to open file", conn);
  }

  return conn;
}

void close_Database(struct Connection *conn) {
  if (conn) {
    if (conn->file) {
      fclose(conn->file);
    } else if (conn->db) {
      free(conn->db);
    }
    free(conn);
  }
}

void write_Database(struct Connection *conn) {
  rewind(conn->file);

  int rc = fwrite(conn->db, sizeof(struct Database), 1, conn->file);
  if (rc != 1) {
    die("Failed to write to database", conn);
  }

  rc = fflush(conn->file);
  if (rc == 1) {
    die("Cannot flush database", conn);
  }
}

void create_Database(struct Connection *conn) {
  int i = 0;

  for (i = 0; i < MAX_ROWS; i++) {
    // make a prototype to initialize it
    struct Address addr = { .id = i, .set = 0 };
    // then just assign it
    conn->db->rows[i] = addr;
  }
}

void set_Database(struct Connection *conn, 
  int id, const char *name, const char *email) {
  
  struct Address *addr = &conn->db->rows[id];

  if (addr->set) {
    die("Already set, delete it first", conn);
  }

  addr->set = 1;
  char *res = strncpy(addr->name, name, MAX_DATA);
  // demonstrate the strncopy bug
  if (!res) {
    die("Name copy failed", conn);
  }

  res = strncpy(addr->email, email, MAX_DATA);
  if (!res) {
    die("Email copy failed", conn);
  }
}

void get_Database(struct Connection *conn, int id) {
  struct Address *addr = &conn->db->rows[id];

  if (addr->set) {
    print_Addresse(addr);
  } else {
    die("ID is not set", conn);
  }
}

void delete_Database(struct Connection *conn, int id) {
  struct Address addr = { .id = id, .set = 0 };
  conn->db->rows[id] = addr;
}

void list_Database(struct Connection *conn) {
  int i = 0;
  int c = 0;
  struct Database *db = conn->db;
  
  for (i = 0; i < MAX_ROWS; i++) {
    struct Address *ptr = &db->rows[i];

    if (ptr->set) {
      print_Addresse(ptr);
      c++;
    }
  }
  if (c == 0) printf("No elements in database\n");
}

void find_Database(struct Connection *conn, char *args[]) {
  int i = 0;
  struct Database *db = conn->db;
  
  for (i = 0; i < MAX_ROWS; i++) {
    struct Address *ptr = &db->rows[i];

    if (ptr->set) {
      if (args[0] && args[1]) {
        if ((strcmp(ptr->name, args[0]) == 0) && (strcmp(ptr->email, args[1]) == 0)) {
          return print_Addresse(ptr);
        }
      } else if (args[0]) {
        if ((strcmp(ptr->name, args[0]) == 0) || (strcmp(ptr->email, args[0]) == 0)) {
          return print_Addresse(ptr);
        }
      }
    }
  }
  die("query not found", conn);
}

int main(int argc, char *argv[]) {
  if (argc < 3) {
    die("USAGE: ex 17 <dbfile> <action> [action params]");
  }

  char *filename = argv[1];
  char action = argv[2][0];
  struct Connection *conn = open_Database(filename, action);
  int id = 0;

  if (argc > 3) id = atoi(argv[3]);
  if (id >= MAX_ROWS) die("There's not that many records.");

  switch (action) {
    case 'c':
      create_Database(conn);
      write_Database(conn);
      break;

    case 'g':
      if (argc != 4) {
        die("Need an id to get", conn);
      }

      get_Database(conn, id);
      break;

    case 's':
      if (argc != 6) {
        die("nedd id, name, email to set.", conn);
      }

      set_Database(conn, id, argv[4], argv[5]);
      write_Database(conn);
      break;
    
    case 'd':
      if (argc != 4) {
        die("Nedd id to delete", conn);
      }
      delete_Database(conn, id);
      write_Database(conn);
      break;

    case 'l':
      list_Database(conn);
      break;

    case 'f':
      if (argc < 4) die("Need at last 1 argument to find", conn);
      char *args[2] = { argv[3], argv[4] };
      find_Database(conn, args);
      break;

    default:
      die("Invalid action: c = create, g = get, s = set, d = delete, l = list", conn);
  }

  close_Database(conn);

  return 0;
}