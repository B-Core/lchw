#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DB=$DIR"/db.dat";

echo "TEST: should create database"
$DIR"/ex17.exe" $DB c
echo "TEST: should create ciro"
$DIR"/ex17.exe" $DB s 0 ciro decaro.ciro@gmail.com
$DIR"/ex17.exe" $DB g 0
echo ""
echo "TEST: should create unautre"
$DIR"/ex17.exe" $DB s 1 unautre un.autre@gmail.com
$DIR"/ex17.exe" $DB g 1

echo ""
echo "TEST: should print 2 elements"
$DIR"/ex17.exe" $DB l

echo ""
echo "TEST: should delete ciro"
$DIR"/ex17.exe" $DB d 0
$DIR"/ex17.exe" $DB g 0
echo ""
echo "TEST: should delete unautre"
$DIR"/ex17.exe" $DB d 1
$DIR"/ex17.exe" $DB g 1

echo ""
echo "TEST: should create 99 elements"
for i in {1..99}
do
  $DIR"/ex17.exe" $DB s $i person-${i} email-${i}@gmail.com
done
echo "99 elements created"

echo ""
echo "TEST: should list 99 elements"
$DIR"/ex17.exe" $DB l

echo ""
echo "TEST: should find person-42"
$DIR"/ex17.exe" $DB f person-42
echo ""
echo "TEST: should find person-42 email-42@gmail.com"
$DIR"/ex17.exe" $DB f person-42 email-42@gmail.com
echo ""
echo "TEST: should not find person"
$DIR"/ex17.exe" $DB f person
echo ""
echo "TEST: should not find person@gmail.com"
$DIR"/ex17.exe" $DB f person@gmail.com
echo ""
echo "TEST: should not find person-42 person@gmail.com"
$DIR"/ex17.exe" $DB f person-42 person@gmail.com

echo ""
echo "TEST: should delete 99 elements"
for i in {1..99}
do
  $DIR"/ex17.exe" $DB d $i
done
echo "99 elements deleted"

echo ""
echo "TEST: list should be empty"
$DIR"/ex17.exe" $DB l

rm $DB

echo ""
echo "TEST COMPLETE"

