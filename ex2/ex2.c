#include <stdio.h>

int main() {

  int age = 25;
  float height = 1.86;
  char *name = "Ciro";

  /*
    %s = pointer to char
    %d = int
    %f = double or float
    %10.4 = specify to printf how to format the number (here it is two digita before the point and 4 after, it could be written .2 to have 2 number after the point and unlimited number before)
    DOC : http://alain.dancel.free.fr/c/c60_10.htm
  */
  printf("I am %s, i am %d years old and i'm %.2f meters tall\n", name, age, height);

  return 0;
}
