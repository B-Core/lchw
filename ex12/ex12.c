#include <stdio.h>

int main(int argc, char *argv[]) {
  int areas[] = { 10, 12, 14, 15, 20 };
  char name[] = "Ciro";
  char full_name[] = {
    'C','i','r','o',' ','D','E',' ','C','A','R','O','\0'
  };
  // WARNING : on some systems you may have to change the %ld to a %u since it will use unsigned int
  printf("The size of an int : %u\n", sizeof(int));
  // sizeof gives the size in byte
  // so calculate the size of an array, aka : length, you have to divide the number of bytes taken by the number of bytes taken for each object type (the size of the type)
  printf("The size of areas in bytes (int[]): %u\n", sizeof(areas));
  // since areas is an array of int
  // to have its length we do that
  printf("The length of areas (int[]) : %d\n", sizeof(areas) / sizeof(int));

  printf("The size of a char : %u\n", sizeof(char));
  printf("The size of name (char[]) : %u\n", sizeof(name));
  printf("The length of name (char[]) : %d\n", sizeof(name) / sizeof(char));

  printf("The size of full_name (char[]) : %u\n", sizeof(full_name));
  printf("The length of name (char[]) : %d\n", sizeof(full_name) / sizeof(char));

  printf("name = %s, full_name = %s\n", name, full_name);

  return 0;
}
